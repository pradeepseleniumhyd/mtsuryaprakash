package com.hcl.mt.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.hcl.mt.commons.Utilities;
import com.relevantcodes.extentreports.ExtentTest;

public class HomePage extends Utilities
{
	
	WebDriver driver;
	ExtentTest test;
	
	public HomePage(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[text()='CONTACT US']")
	public WebElement contactUs;
	
	
	
	
	public void click_contactUS(ExtentTest test)
	{
		click(driver, test, contactUs, "CONTACT US");
	}
	
	

}
