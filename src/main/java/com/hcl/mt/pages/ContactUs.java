package com.hcl.mt.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.hcl.mt.commons.Utilities;
import com.relevantcodes.extentreports.ExtentTest;

public class ContactUs extends Utilities
{
	WebDriver driver;
	
	public ContactUs(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="(//b[text()='TOOLS PLUS']/following::a)[1]")
	public WebElement contactUs_AddressLine;
	
	@FindBy(xpath="//h3[text()='Shipping Questions:']")
	public WebElement contactUs_ShippingQuestions;
	
	@FindBy(xpath="//h3[text()='Product Catalog Questions:']")
	public WebElement contactUs_ProductCatalogQuestions;
	
	@FindBy(xpath="//b[text()='Retail Store Hours:']")
	public WebElement contactUs_RetailStoreHours;
	
	
	@FindBy(xpath="//b[text()='Retail Store Hours:']/../..")
	public WebElement contactUs_RSHoursInfo;
	
	
	@FindBy(xpath="(//h3[text()='Shipping Questions:']/following-sibling::dl/dt/a)[1]")
	public WebElement contactUs_ShippingQuestions1;
	
	@FindBy(xpath="(//h3[text()='Shipping Questions:']/following-sibling::dl/dt/a)[2]")
	public WebElement contactUs_ShippingQuestions2;
	
	@FindBy(xpath="(//h3[text()='Shipping Questions:']/following-sibling::dl/dt/a)[3]")
	public WebElement contactUs_ShippingQuestions3;
	
	@FindBy(xpath="(//h3[text()='Shipping Questions:']/following-sibling::dl/dt/a)[4]")
	public WebElement contactUs_ShippingQuestions4;
	
	@FindBy(xpath="(//h3[text()='Shipping Questions:']/following-sibling::dl/dt/a)[5]")
	public WebElement contactUs_ShippingQuestions5;
	
	@FindBy(xpath="(//h3[text()='Shipping Questions:']/following-sibling::dl/dt/a)[6]")
	public WebElement contactUs_ShippingQuestions6;
	
	
	
	public void validationOfContactUs(ExtentTest test) throws Exception
	{
		Thread.sleep(5000);
		validateData(driver, test, contactUs_AddressLine, "153 Meadow Street");
		validateData(driver, test, contactUs_AddressLine, "Waterbury, CT USA 06702");
		validateData(driver, test, contactUs_ShippingQuestions, "Shipping Questions:");
		validateData(driver, test, contactUs_ProductCatalogQuestions, "Product Catalog Questions:");
		validateData(driver, test, contactUs_RetailStoreHours, "Retail Store Hours:");
		validateData(driver, test, contactUs_RSHoursInfo, "8:00am to 5:00pm Monday through Friday");
		validateData(driver, test, contactUs_RSHoursInfo, "TEMPORARILY CLOSED ON SATURDAYS");
		validateData(driver, test, contactUs_RSHoursInfo, "Closed on Sundays and Holidays");
	}
	
	
	public void shippingQuestions(ExtentTest test) throws Exception
	{
		Thread.sleep(5000);
		Thread.sleep(5000);
		validateData(driver, test, contactUs_ShippingQuestions, "Shipping Questions:");
		validateData(driver, test, contactUs_ShippingQuestions1, "How much are shipping charges?");
		validateData(driver, test, contactUs_ShippingQuestions2, "Can I track my order prior to shipping?");
		validateData(driver, test, contactUs_ShippingQuestions3, "Do you ship to Canada?");
		validateData(driver, test, contactUs_ShippingQuestions4, "What about other international shipments?");
		validateData(driver, test, contactUs_ShippingQuestions5, "How long will it take to get my Gift Certificate delivered?");
		validateData(driver, test, contactUs_ShippingQuestions6, "Why is this item on backorder? When I placed my order, it showed as being in-stock.");


	}
	
	
	

}
