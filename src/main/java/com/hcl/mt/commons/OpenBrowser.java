package com.hcl.mt.commons;

import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class OpenBrowser 
{
	
	String serverName="local";
	
	public WebDriver getBrowser(WebDriver driver,ExtentTest test, String browserName)
	{
		try
		{
		if(browserName.equalsIgnoreCase("chrome"))
		{
			if(serverName.equalsIgnoreCase("remote"))
			{
				DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
				desiredCapabilities.setBrowserName("chrome");
				desiredCapabilities.setPlatform(Platform.ANY);
				driver = new RemoteWebDriver(new URL("http://192.168.29.122:4444/wd/hub"), desiredCapabilities);
			}else
			{
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "/Drivers/chromedriver.exe");
			driver = new ChromeDriver();
			System.out.println("Chrome browser is opened");
			Reporter.log("Chrome browser is opened");
			test.log(LogStatus.PASS, browserName +" browser is opened");
			}
		}else if(browserName.equalsIgnoreCase("ie"))
		{
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+ "/Drivers/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			System.out.println("IE browser is opened");
			test.log(LogStatus.PASS, browserName +" browser is opened");

			
		}else if(browserName.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+ "/Drivers/geckodriver.exe");
			//driver = new FirefoxDriver();
			System.out.println("Firefox browser is opened");
			test.log(LogStatus.PASS, browserName +" browser is opened");


		}else
		{
			System.out.println(browserName + " is not matched");
			test.log(LogStatus.FAIL, browserName +" browser is opened");

		}
		
		}catch(Exception e)
		{
			System.out.println("Browser code is failing " + e.getMessage());
			test.log(LogStatus.FAIL, browserName +" browser is not opened" + e.getMessage());

		}
		
		
		return driver;
		
	}

}
