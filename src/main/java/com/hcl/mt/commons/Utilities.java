package com.hcl.mt.commons;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Utilities 
{
	
	public static ExtentReports extent;
	public com.relevantcodes.extentreports.ExtentTest test;
	
	//Common methods - url, click, dropdown, title, validate etc.,
	
	
	public void navigateURL(WebDriver driver,ExtentTest test,String url) throws Exception
	{
		try
		{
			driver.get(url);
			Thread.sleep(5000);		
			test.log(LogStatus.PASS, url + " is entered");
		}catch(Exception e)
		{
			System.out.println("Navigate URL method is failing " + e.getMessage());
			test.log(LogStatus.FAIL, url + "is not entered " + e.getMessage());
		}
		
	}
	
	public void click(WebDriver driver,ExtentTest test, String xpathvalue, String value)
	{
		try
		{
			driver.findElement(By.xpath(xpathvalue)).click();
			System.out.println(value + "is clicked");
			Reporter.log(value + "is clicked");
			test.log(LogStatus.PASS, value + "is clicked");
		}catch(Exception e)
		{
			System.out.println("Click method is failing " + e.getMessage());
			Reporter.log(value + "is not clicked " + e.getMessage());
			test.log(LogStatus.FAIL, value + "is not clicked " + e.getMessage());
		}
	}
	
	public void click(WebDriver driver,ExtentTest test, WebElement xpathvalue, String value)
	{
		try
		{
			xpathvalue.click();
			System.out.println(value + "is clicked");
			Reporter.log(value + "is clicked");
			test.log(LogStatus.PASS, value + "is clicked");
		}catch(Exception e)
		{
			System.out.println("Click method is failing " + e.getMessage());
			Reporter.log(value + "is not clicked " + e.getMessage());
			test.log(LogStatus.FAIL, value + "is not clicked " + e.getMessage());
		}
	}
	
	public void enterData(WebDriver driver,ExtentTest test, String xpathvalue, String value, String textboxname)
	{
		try
		{
			driver.findElement(By.xpath(xpathvalue)).clear();
			test.log(LogStatus.PASS, textboxname + " textbox content is cleared" );
			driver.findElement(By.xpath(xpathvalue)).sendKeys(value);
			test.log(LogStatus.PASS, value + " is entered in " + textboxname );
			System.out.println(value + "is entered");
		}catch(Exception e)
		{
			System.out.println("Enter Data method is failing " + e.getMessage());
			test.log(LogStatus.FAIL, "Enter Data method is failing " + e.getMessage());
		}
	}
	
	
	public void validateData(WebDriver driver,ExtentTest test, String xpathvalue, String ExpectedValue)
	{
		try
		{
			String actualValue = driver.findElement(By.xpath(xpathvalue)).getText();
			
			if(actualValue.contains(ExpectedValue))
			{
				test.log(LogStatus.PASS, ExpectedValue + " is matched with " + actualValue);
			}else
			{
				test.log(LogStatus.FAIL, ExpectedValue + " is not matched with " + actualValue);

			}
		
		}catch(Exception e)
		{
			System.out.println("Validate Data is failing " + e.getMessage());
			test.log(LogStatus.FAIL, "Validate Data method is failing " + e.getMessage());

		}
		
	}
	
	public void validateData(WebDriver driver,ExtentTest test, WebElement xpathvalue, String ExpectedValue)
	{
		try
		{
			String actualValue = xpathvalue.getText();
			
			if(actualValue.contains(ExpectedValue))
			{
				
				takeScreenshot(driver,  actualValue+ExpectedValue);
				test.log(LogStatus.PASS, ExpectedValue + " is matched with " + actualValue + test.addScreenCapture(System.getProperty("user.dir")+"/Screenshots/"+ExpectedValue+".png"));
				
			}else
			{
				test.log(LogStatus.FAIL, ExpectedValue + " is not matched with " + actualValue);

			}
		
		}catch(Exception e)
		{
			System.out.println("Validate Data is failing " + e.getMessage());
			test.log(LogStatus.FAIL, "Validate Data method is failing " + e.getMessage());

		}
		
	}

	public void takeScreenshot(WebDriver driver,String name) throws Exception
	{

		
		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		 // now copy the  screenshot to desired location using copyFile //method
		FileUtils.copyFile(src, new File(System.getProperty("user.dir")+"/Screenshots/"+name+".png"));
	}
	
	
}
