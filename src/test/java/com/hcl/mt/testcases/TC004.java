package com.hcl.mt.testcases;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.hcl.mt.commons.OpenBrowser;
import com.hcl.mt.commons.Utilities;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class TC004 extends Utilities
{
	WebDriver driver;	
	OpenBrowser openBrowser = new OpenBrowser();
	
	@BeforeSuite
	public void report()
	{
		extent = new ExtentReports(System.getProperty("user.dir")+"/Reports/MTTools.html", true);
	}
	
	
	
	@Parameters({"browserName","urlvalue"})
	@Test
	public void tc004(String browserName, String urlvalue) throws Exception
	{
			
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\TestData\\MT.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet("TestData");
			System.out.println(sheet.getLastRowNum());

			for(int i = 1; i<= sheet.getLastRowNum(); i ++)
			{
				
				if(sheet.getRow(i).getCell(0).getStringCellValue().equals("TC004") && sheet.getRow(i).getCell(2).getStringCellValue().equalsIgnoreCase("Yes"))
				{	
					 test = extent.startTest("TC004", "Tools Search");
					driver=openBrowser.getBrowser(driver,test, browserName);
					navigateURL(driver,test, urlvalue);
					
					click(driver,test, "//a[text()='ABOUT']", "ABOUT");
					enterData(driver,test, "//input[@class='search-input']", "Inverter", "Search");
					click(driver,test, "//input[@class='search-btn']", "Search");
					extent.flush();
					extent.endTest(test);
					
				}
				
				if(sheet.getRow(i).getCell(0).getStringCellValue().equals("TC004") && sheet.getRow(i).getCell(2).getStringCellValue().equalsIgnoreCase("No"))
				{
					 test = extent.startTest("TC004", "Tools Search");
					 test.log(LogStatus.SKIP, "TC004 Run Mode mentioned as No");
					 extent.flush();
						extent.endTest(test);
					throw new SkipException("TC004 Run Mode mentioned as No");			
				}
				
				
			}
				
		
	}
	
	
	@AfterMethod
	public void am()
	{
		try
		{
			driver.quit();
		}catch(Exception e)
		{
			System.out.println("Driver is already closed");
		}
	}

}
