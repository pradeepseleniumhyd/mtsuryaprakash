package com.hcl.mt.testcases;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.hcl.mt.commons.OpenBrowser;
import com.hcl.mt.commons.Utilities;

public class TC003 extends Utilities
{
	WebDriver driver;
	OpenBrowser openBrowser = new OpenBrowser();
	
	@Parameters({"browserName","urlvalue"})
	@Test
	public void tc003(String browserName, String urlvalue) throws Exception
	{
			
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\TestData\\MT.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet("TestData");
			System.out.println(sheet.getLastRowNum());

			for(int i = 1; i<= sheet.getLastRowNum(); i ++)
			{
				if(sheet.getRow(i).getCell(0).getStringCellValue().equals("TC003") && sheet.getRow(i).getCell(2).getStringCellValue().equalsIgnoreCase("Yes"))
				{	
					driver=openBrowser.getBrowser(driver,test, browserName);
					navigateURL(driver,test, urlvalue);
					click(driver,test, "//a[text()='ABOUT']", "ABOUT");
					enterData(driver,test, "//input[@class='search-input']", "Inverter", "Search");
					click(driver,test, "//input[@class='search-btn']", "Search");
				}
				
				if(sheet.getRow(i).getCell(0).getStringCellValue().equals("TC003") && sheet.getRow(i).getCell(2).getStringCellValue().equalsIgnoreCase("No"))
				{
					throw new SkipException("TC003 Run Mode mentioned as No");
				}
			}
				
		
	}
	
	
	@AfterMethod
	public void am()
	{
		try
		{
			driver.quit();
		}catch(Exception e)
		{
			System.out.println("Driver is already closed");
		}
	}

}
