package com.hcl.mt.testcases;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.hcl.mt.commons.OpenBrowser;
import com.hcl.mt.commons.Utilities;
import com.hcl.mt.pages.ContactUs;
import com.hcl.mt.pages.HomePage;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class TC007 extends Utilities
{
	WebDriver driver;	
	OpenBrowser openBrowser = new OpenBrowser();
	
	@BeforeSuite
	public void report()
	{
		extent = new ExtentReports(System.getProperty("user.dir")+"/Reports/MTTools.html", true);
	}
	
	
	
	@Parameters({"browserName","urlvalue"})
	@Test
	public void tc007(String browserName, String urlvalue) throws Exception
	{
			
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\TestData\\MT.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet("TestData");
			System.out.println(sheet.getLastRowNum());

			for(int i = 1; i<= sheet.getLastRowNum(); i ++)
			{
				
				if(sheet.getRow(i).getCell(0).getStringCellValue().equals("TC007") && sheet.getRow(i).getCell(2).getStringCellValue().equalsIgnoreCase("Yes"))
				{	
					 test = extent.startTest("TC007", "Tools - Contact Us Page - Shipping Validation");
					driver=openBrowser.getBrowser(driver,test, browserName);
					navigateURL(driver,test, urlvalue);
					
					HomePage homePage = new HomePage(driver);
					homePage.click_contactUS(test);
					
					ContactUs contactUs = new ContactUs(driver);
					contactUs.shippingQuestions(test);
					
					
					extent.flush();
					extent.endTest(test);
					
				}
				
				if(sheet.getRow(i).getCell(0).getStringCellValue().equals("TC007") && sheet.getRow(i).getCell(2).getStringCellValue().equalsIgnoreCase("No"))
				{
					 test = extent.startTest("TC007", "Tools - Contact Us Page Validation");
					 test.log(LogStatus.SKIP, "TC007 Run Mode mentioned as No");
					 extent.flush();
						extent.endTest(test);
					throw new SkipException("TC007 Run Mode mentioned as No");			
				}
				
				
			}
				
		
	}
	
	
	@AfterMethod
	public void am()
	{
		try
		{
			driver.quit();
		}catch(Exception e)
		{
			System.out.println("Driver is already closed");
		}
	}

}
