package com.hcl.mt.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC001 
{
	WebDriver driver;

	@BeforeMethod
	public void beforeMethod()
	{
		try
		{
		System.out.println("Browser Code");
		System.setProperty("webdriver.chrome.driver", "D:/Surya Prakash/Code/JavaBasics1/Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://newtours.demoaut.com/");
		Thread.sleep(5000);
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
	}
	

	@Test
	public void tc001()
	{
		try
		{
		System.out.println("Main Code for TC001");
		Boolean signon=driver.findElement(By.xpath("//a[text()='SIGN-ON']")).isDisplayed();
		System.out.println(signon);
		if(signon)
		{
			System.out.println("Sign On is displayed");
		}else
		{
			System.out.println("Sign on is not displayed");
		}
		
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	
	@AfterMethod
	public void afterMethod()
	{
		try
		{
		System.out.println("driver close");
		driver.quit();
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	
}
