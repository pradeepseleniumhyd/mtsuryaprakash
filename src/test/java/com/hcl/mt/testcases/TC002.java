package com.hcl.mt.testcases;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class TC002 
{
	WebDriver driver;
	
	@Test
	public void tc002() throws Exception
	{
			
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\TestData\\MT.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet("TestData");
			System.out.println(sheet.getLastRowNum());

			for(int i = 1; i< sheet.getLastRowNum(); i ++)
			{
				if(sheet.getRow(i).getCell(0).getStringCellValue().equals("TC002") && sheet.getRow(i).getCell(2).getStringCellValue().equalsIgnoreCase("Yes"))
				{
					System.out.println("Browser Code");
					System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "/Drivers/chromedriver.exe");
					driver = new ChromeDriver();
					driver.get("http://newtours.demoaut.com/");
					Thread.sleep(5000);
					System.out.println("Main Code for TC001");
					Boolean signon=driver.findElement(By.xpath("//a[text()='SIGN-ON']")).isDisplayed();
					System.out.println(signon);
					if(signon)
					{
						System.out.println("Sign On is displayed");
					}else
					{
						System.out.println("Sign on is not displayed");
					}
				
				}
				
				if(sheet.getRow(i).getCell(0).getStringCellValue().equals("TC002") && sheet.getRow(i).getCell(2).getStringCellValue().equalsIgnoreCase("No"))
				{
					throw new SkipException("TC002 Run Mode mentioned as No");
				}
			}
				
		
	}
	
	
	@AfterMethod
	public void am()
	{
		try
		{
			driver.quit();
		}catch(Exception e)
		{
			System.out.println("Driver is already closed");
		}
	}

}
